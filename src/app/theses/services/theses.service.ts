import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Injectable({
  providedIn: 'root'
})
export class ThesesService {

  constructor(private _context: AngularDataContext) { }

  getCompletedTheses(): any {
    return this._context.model('Instructors/Me/theses')
      .asQueryable()
      .expand('type,students($expand=student($select=StudentSummary)),status')
      .where('status/alternateName').equal('completed')
      .orderByDescending('startYear')
      .thenByDescending('startPeriod')
      .take(-1)
      .getItems();
  }

  getCurrentTheses(): any {
    return this._context.model('Instructors/Me/activetheses')
      .asQueryable()
      .expand('type,students($expand=student($select=StudentSummary)),status')
      .orderByDescending('startYear')
      .thenByDescending('startPeriod')
      .take(-1)
      .getItems();
  }

}
