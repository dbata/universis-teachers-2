import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(private context: AngularDataContext) { }


  getStudentBySearch(searchText): any {
    return this.context.model('instructors/me/classes/students')
      .asQueryable()
      .expand('examPeriod,student($expand=studentStatus,inscriptionYear,studyProgram($expand=studyLevel)),courseClass($expand=course,period,year),status')
      .where('student/person/familyName').contains(searchText)
      .or('student/person/givenName').contains(searchText)
      .or('student/studentIdentifier').contains(searchText)
      .take(-1)
      .getItems();

  }

  getThesesBySearch(searchText): any {
    return this.context.model('Instructors/Me/theses/students')
      .asQueryable()
      .expand('student($expand=person,studentStatus,inscriptionYear,studyProgram($expand=studyLevel)),thesis($expand=startPeriod,startYear,type)')
      .where('student/person/familyName').contains(searchText)
      .or('student/person/givenName').contains(searchText)
      .or('student/studentIdentifier').contains(searchText)
      .take(-1)
      .getItems();
  }


}
