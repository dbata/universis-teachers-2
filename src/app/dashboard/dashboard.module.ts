import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {ChartsModule} from 'ng2-charts/ng2-charts';

import {DashboardComponent} from './dashboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {CommonModule} from '@angular/common';
import { InstructorOpenGradesComponent } from './components/instructor-open-grades/instructor-open-grades.component';
import { InstructorCurrentCoursesComponent } from './components/instructor-current-courses/instructor-current-courses.component';
import {environment} from '../../environments/environment';
import {NgPipesModule} from 'ngx-pipes';
import {CoursesSharedModule} from '../courses/courses-shared.module';
import { InstructorRecentUploadsComponent } from './components/instructor-recent-uploads/instructor-recent-uploads.component';
import {SharedModule} from '@universis/common';
import {EventsModule} from '@universis/ngx-events';
import { QaModule } from '@universis/ngx-qa';

@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        CoursesSharedModule,
        ChartsModule,
        TranslateModule,
        MostModule,
        NgPipesModule,
        SharedModule,
        EventsModule,
        QaModule
    ],
    declarations: [
        DashboardComponent,
        InstructorOpenGradesComponent,
        InstructorCurrentCoursesComponent,
        InstructorRecentUploadsComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/dashboard.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}
