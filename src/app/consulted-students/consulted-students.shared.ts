import { CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { SharedModule } from '@universis/common';
import { SendMessageActionComponent } from './components/send-message-action/send-message-action.component';
import {environment} from '../../environments/environment';
import { RouterModalModule } from '@universis/common/routing';
import { MessagesSharedModule } from '../messages/messages.shared';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    RouterModalModule,
    MessagesSharedModule
  ],
  declarations: [
    SendMessageActionComponent
  ],
  exports: [
    SendMessageActionComponent
  ],
  entryComponents: [
    SendMessageActionComponent
  ],
  providers: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConsultedStudentsSharedModule {
    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch(err => {
          console.error('An error occurred while loading translations');
          console.error(err);
        });
      }
      static forRoot(): ModuleWithProviders {
        return {
          ngModule: ConsultedStudentsSharedModule,
          providers: [
          ],
        };
      }
      // tslint:disable-next-line:use-life-cycle-interface
      async ngOnInit() {
        environment.languages.forEach(language => {
          import(`./i18n/consulted-students.${language}.json`).then((translations) => {
            this._translateService.setTranslation(language, translations, true);
          });
        });
      }
}

