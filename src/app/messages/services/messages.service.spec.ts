import { TestBed } from '@angular/core/testing';
import { MessagesService } from './messages.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ApiTestingModule, ApiTestingController, TestingConfigurationService} from '@universis/common/testing';
import {MostModule} from '@themost/angular';
import {ConfigurationService} from '@universis/common';


describe('MessagesService', () => {
  let mockApi: ApiTestingController;
  beforeEach(async () => {
    return TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
      MostModule.forRoot( {
        base: '/',
        options: {
          useMediaTypeExtensions: false
        }
      }),
      ApiTestingModule.forRoot()
    ],
    providers: [
      {
        provide: ConfigurationService,
        useClass: TestingConfigurationService
      },
      MessagesService
    ],
    declarations: []
  }).compileComponents();
  });
  beforeEach( () => {
    mockApi = TestBed.get(ApiTestingController);
    /*
     * TODO: Add tests for the methods and use mockApi to return
     *  mock data and
     */
  });

  it('should be created', () => {
    const service: MessagesService = TestBed.get(MessagesService);
    expect(service).toBeTruthy();
  });
});
