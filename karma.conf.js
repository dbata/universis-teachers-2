// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

const puppeteer = require('../node_modules/puppeteer');
process.env.CHROME_BIN = puppeteer.executablePath();
process.env.CHROMIUM_BIN = puppeteer.executablePath();


module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client:{
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      reports: ['html', 'text-summary'],
      fixWebpackSourcePaths: true
    },
    angularCli: {
      environment: 'dev'
    },
    reporters: ['progress', 'kjhtml', 'coverage-istanbul'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeNoSandbox', 'ChromeHeadlessNoSandbox'],
    captureTimeout: 100000,
    browserDisconnectTolerance: 3,
    browserDisconnectTimeout : 100000,
    browserNoActivityTimeout : 100000,
    customLaunchers: {
      ChromeNoSandbox: {
        base: 'Chrome',
        flags: [
          '--no-sandbox',
          '--enable-logging=stderr',
          '--disable-web-security',
          '--disable-gpu',
          '--no-proxy-server'
      ]
    },
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: [
          '--no-sandbox',
          '--enable-logging=stderr',
          '--disable-web-security',
          '--disable-gpu',
          '--no-proxy-server'
      ]
      }
    },
    singleRun: false
  });
};
